#include <errno.h>
#include <netdb.h>
#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <unistd.h>
#include <math.h>

#include "cJSON.h"

static cJSON *ping_msg();
static cJSON *join_msg(char *bot_name, char *bot_key);
static cJSON *throttle_msg(double throttle);
static cJSON *make_msg(char *type, cJSON *msg);

static cJSON *read_msg(int fd);
static void write_msg(int fd, cJSON *msg);

static void InitializeRace(cJSON *json, cJSON *piecesArray);
static cJSON *get_throttle_position(cJSON *json, cJSON *piecesArray);
static double calculateCentrifugalForce(double radius, double velocity);
static int useTurboIfAvailable(int index, cJSON *piecesArray);
static void parseTurboMessage(cJSON *json);
static cJSON *turbo_msg();

static int turboAvailable = 0;
// Define the maximum slip angle for car
static double maximumCarSlipAngle = 45.0; 
static double maximumCarSlipAngleEasingDown = 40.0;
// Previously calculated slip angle of car
static double previousCarSlipAngle = 0.0;
// Define the point when car will crash * Have to change if car crashes
static double maximumCentrifugalForce = 25.0; 
// Point when throttle should be ease down to prevent crash
static double triggerCentrifugalForce = 20.0; 
// Previously calculated force to check whether we should slow down or keep the gas position
static double previousCentrifugalForce = 0.0;
static double throttle = 0.6;
// Define point when slip and CForce create crash
static double maximumCentrifugalForceSlipAngle = 1000.0;

static double previousCentriSlipForce = 0.0;

static double maxCorneringSpeed = 7.0;

static void error(char *fmt, ...)
{
    char buf[BUFSIZ];
    
    va_list ap;
    va_start(ap, fmt);
    vsnprintf(buf, BUFSIZ, fmt, ap);
    va_end(ap);
    
    if (errno)
        perror(buf);
    else
        fprintf(stderr, "%s\n", buf);
    
    exit(1);
}

static int connect_to(char *hostname, char *port)
{
    int status;
    int fd;
    char portstr[32];
    struct addrinfo hint;
    struct addrinfo *info;
    
    memset(&hint, 0, sizeof(struct addrinfo));
    hint.ai_family = PF_INET;
    hint.ai_socktype = SOCK_STREAM;
    
    sprintf(portstr, "%d", atoi(port));
    
    status = getaddrinfo(hostname, portstr, &hint, &info);
    if (status != 0) error("failed to get address: %s", gai_strerror(status));
    
    fd = socket(PF_INET, SOCK_STREAM, 0);
    if (fd < 0) error("failed to create socket");
    
    status = connect(fd, info->ai_addr, info->ai_addrlen);
    if (status < 0) error("failed to connect to server");
    
    freeaddrinfo(info);
    return fd;
}

static void log_message(char *msg_type_name, cJSON *msg)
{
    cJSON *msg_data;
    
    if (!strcmp("join", msg_type_name)) {
        puts("Joined");
    } else if (!strcmp("gameStart", msg_type_name)) {
        puts("Race started");
    } else if (!strcmp("crash", msg_type_name)) {
        puts("Someone crashed");
    } else if (!strcmp("gameEnd", msg_type_name)) {
        puts("Race ended");
    } else if (!strcmp("error", msg_type_name)) {
        msg_data = cJSON_GetObjectItem(msg, "data");
        if (msg_data == NULL)
            puts("Unknown error");
        else
            printf("ERROR: %s\n", msg_data->valuestring);
    }
}

int main(int argc, char *argv[])
{
    int sock;
    cJSON *json;
    
    if (argc != 5)
        error("Usage: bot host port botname botkey\n");
    
    sock = connect_to(argv[1], argv[2]);
    
    json = join_msg(argv[3], argv[4]);
    write_msg(sock, json);
    cJSON_Delete(json);
	cJSON *piecesArray = cJSON_CreateArray();
    
    while ((json = read_msg(sock)) != NULL) {
        cJSON *msg, *msg_type;
        char *msg_type_name;
        
        
        msg_type = cJSON_GetObjectItem(json, "msgType");
        if (msg_type == NULL)
            error("missing msgType field");
        
        msg_type_name = msg_type->valuestring;
        if (!strcmp("carPositions", msg_type_name)) {
            msg = get_throttle_position(json, piecesArray);
        } else if (!strcmp("turboAvailable", msg_type_name)) {
			//printf(cJSON_Print(json));
			printf("*********************** Turbo arrived\n");
			turboAvailable = 1;
			msg = ping_msg();
		} else if (!strcmp("gameInit", msg_type_name)) {
			InitializeRace(json, piecesArray);
			msg = ping_msg();
		} else {
            log_message(msg_type_name, json);
            msg = ping_msg();
        }
        
        write_msg(sock, msg);
        
        cJSON_Delete(msg);
        cJSON_Delete(json);
    }
	
	cJSON_Delete(piecesArray);
    
    return 0;
}

static void parseTurboMessage(cJSON *json)
{
	//cJSON *raceData = cJSON_GetObjectItem(data, "race");
}

static void InitializeRace(cJSON *json, cJSON *piecesArray)
{
	cJSON *data = cJSON_GetObjectItem(json, "data");
	if (data != NULL)
	{
		cJSON *raceData = cJSON_GetObjectItem(data, "race");
		cJSON *trackData = cJSON_GetObjectItem(raceData, "track");
		cJSON *piecesData = cJSON_GetObjectItem(trackData, "pieces");
		if (piecesData != NULL)
		{
			//printf(cJSON_Print(piecesData));
			int arraySize = cJSON_GetArraySize(piecesData);
			int i;
			for (i = 0; i < arraySize; i++)
			{
				cJSON *piece = cJSON_GetArrayItem(piecesData, i);
				cJSON_AddItemToArray(piecesArray, cJSON_Duplicate(piece,1));
			}
		}
	}
}

// 1 use
// 0 do not use
static int useTurboIfAvailable(int index, cJSON *piecesArray)
{
	int iterations = 2;
	index += 1;
	if (index <= (cJSON_GetArraySize(piecesArray) - 1))
	{
		int i = 0;
		for (i = index; i < cJSON_GetArraySize(piecesArray) - 1; ++i)
		{
			cJSON *currentPiece = cJSON_GetArrayItem(piecesArray, i);
			if (cJSON_GetObjectItem(currentPiece, "length") == NULL)
			{
				return 0;
			}
			if (iterations == 0)
			{
				break;
			}
			iterations--;
		}
	}
	
	if (iterations > 0)
	{
		int i = 0;
		for (i = 0; i < cJSON_GetArraySize(piecesArray); ++i)
		{
			cJSON *currentPiece = cJSON_GetArrayItem(piecesArray, i);
			if (cJSON_GetObjectItem(currentPiece, "length") == NULL)
			{
				return 0;
			}
			if (iterations == 0)
			{
				break;
			}
			iterations--;
		} 
	}
	return 1;
}

static cJSON *get_throttle_position(cJSON *json, cJSON *piecesArray)
{
	static int currentIndex = -1;
	cJSON *data = cJSON_GetObjectItem(json, "data");
	if (data != NULL)
	{
		cJSON *player = cJSON_GetArrayItem(data, 0);
		if (player != NULL)
		{
			cJSON *piecePositionData = cJSON_GetObjectItem(player, "piecePosition");
			cJSON *pieceIndexData = cJSON_GetObjectItem(piecePositionData, "pieceIndex");
            cJSON *angleData = cJSON_GetObjectItem(player, "angle");
	
			double slipAngle = 0;
			if (angleData != NULL) {
				slipAngle = angleData->valuedouble;
			}
			
			//angle = abs(angle);
            
			int pieceIndex = pieceIndexData->valueint;
			static cJSON *currentPiece = NULL;
            static cJSON *nextPiece = NULL;
            
			if (pieceIndex != currentIndex)
			{
				currentIndex = pieceIndex;
					int useTurbo = useTurboIfAvailable(currentIndex, piecesArray);
					if (useTurbo == 0)
					{
						printf("DO NOT USE TURBO!");
					} else {
						printf("USE TURBO, NOW!");
					}				
                currentPiece = cJSON_GetArrayItem(piecesArray, currentIndex);
				if (currentIndex < (cJSON_GetArraySize(piecesArray) + 1)) {
                    printf("Get next piece %d\n", currentIndex);
					nextPiece = cJSON_GetArrayItem(piecesArray, currentIndex + 1);
				} else {
                    //printf("back to first piece\n");
					nextPiece = cJSON_GetArrayItem(piecesArray, 1);
				}
			} else {
            
            }
            if (currentPiece == NULL)
            {
				currentPiece = cJSON_GetArrayItem(piecesArray, 0);
            }
			if (nextPiece == NULL)
			{
				nextPiece = cJSON_GetArrayItem(piecesArray, 1);
			}
            cJSON *length = cJSON_GetObjectItem(currentPiece, "length");
			cJSON *nextLength = cJSON_GetObjectItem(nextPiece, "length");

            static double prevInPieceDistance = 0;
			cJSON *distanceInPiece = cJSON_GetObjectItem(piecePositionData, "inPieceDistance");
			double distance = 0;
			if (distanceInPiece != NULL) {
				distance = distanceInPiece->valuedouble;
			}
            double velocity = (distance - prevInPieceDistance);
            prevInPieceDistance = distance;
			
			cJSON *radiusData = cJSON_GetObjectItem(currentPiece, "radius");
			double radius = 0;
			if (radiusData != NULL) {
				radius = radiusData->valuedouble;
			}
			
            cJSON *pieceAngleData = cJSON_GetObjectItem(nextPiece, "angle");
            double pieceAngle = 0.0;
            if (pieceAngleData != NULL) {
                pieceAngle = pieceAngleData->valuedouble;
                pieceAngle = abs(pieceAngle);
            }
			
			double centrifugalForce,absoluteSlipAngle,centriSlipForce = 0;
			if (velocity > 0) 
			{
				if (radius != 0) {
					centrifugalForce = calculateCentrifugalForce(radius, velocity);
					absoluteSlipAngle = abs(slipAngle);
					centriSlipForce = centrifugalForce * absoluteSlipAngle;
					printf("Current centriSlipForce %f with car slide angle %f\n", centriSlipForce, slipAngle);
				}
			}
            
			if (length != NULL && nextLength != NULL) {
				throttle = 1.0;
            } else if (length != NULL && nextLength == NULL) {
                if ((pieceAngle >= 40 && distance > 30.0 && velocity > maxCorneringSpeed) ||(velocity > 12)) {
                    //printf("Braking 0.0 entering corner\n");
                    throttle = 0.0;
                } else {
                    //printf("Full gas entring corner\n");
                    throttle = 1.0;
                }
            } else if (length == NULL && nextLength != NULL){
			// TODO Implement centriSlipForce here also?
			    printf("Exiting corner\n");
                if (distance > 60.0) {
					int useTurbo = useTurboIfAvailable(currentIndex, piecesArray);
					if (useTurbo == 0)
					{
						printf("ALA LAITA!");
					} else {
						printf("KAYTA SE PRKLE!");
					}
					if (turboAvailable == 1 && useTurbo == 1)
					{
						printf("NYT ON KAYTETTY!\n");
						turboAvailable = 0;
						return turbo_msg();
					} else {
						throttle = 1.0;
					}
                } else if (slipAngle < 35.0){
                    throttle = 0.7;
                }
            } else if (length == NULL && nextLength == NULL) {
				//printf("Cornering\n");
				if (centriSlipForce < 100 || (previousCentriSlipForce > centriSlipForce && centriSlipForce < 200))
				{
					throttle = 0.7;
				} else if (centriSlipForce > 100 && centriSlipForce < 200)
				{
					throttle = 0.5;
				} else {
					throttle = 0.2;
				}
				previousCentrifugalForce = centrifugalForce;
				previousCarSlipAngle = absoluteSlipAngle;
				previousCentriSlipForce = centriSlipForce;
				printf("Current throttle value %f and velocity %f\n", throttle, velocity);
            } else {
                throttle = 0.4;
            }
			
		}
	}
	if (throttle >= 1.0) {
		throttle = 1.0;
	} else if (throttle <= 0.0) {
		throttle = 0.0;
	}
	//printf("Current throttle value %f\n", throttle);
	return throttle_msg(throttle);
}

static double pi = 3.1415; 	// Pii
static double mass = 1.0;		// Auton massa

static double calculateCentrifugalForce(double radius, double velocity)
{
	//printf("calculating centrifugal force with radiud %f and velocity %f\n", radius, velocity);
	double diameter = radius * 2;
	double circumferenceVelocity = (pi * diameter * velocity) / 100; 	// Kehänopeus
	// 39.26875
	double centrifugalForce = mass * (pow(circumferenceVelocity, 2)) / radius;	// Keskipakoisvoima
	// 61.68139
	return centrifugalForce;
}

static cJSON *ping_msg()
{
    return make_msg("ping", cJSON_CreateString("ping"));
}

static cJSON *join_msg(char *bot_name, char *bot_key)
{
    cJSON *data = cJSON_CreateObject();
    cJSON_AddStringToObject(data, "name", bot_name);
    cJSON_AddStringToObject(data, "key", bot_key);
    
    return make_msg("join", data);
}

static cJSON *turbo_msg()
{
	printf("Sendin teh turbo message");
	return make_msg("turbo", cJSON_CreateString("Laetkae pohjaan!"));
}

static cJSON *throttle_msg(double throttle)
{
    return make_msg("throttle", cJSON_CreateNumber(throttle));
}

static cJSON *make_msg(char *type, cJSON *data)
{
    cJSON *json = cJSON_CreateObject();
    cJSON_AddStringToObject(json, "msgType", type);
    cJSON_AddItemToObject(json, "data", data);
    return json;
}

static cJSON *read_msg(int fd)
{
    int bufsz, readsz;
    char *readp, *buf;
    cJSON *json = NULL;
    
    bufsz = 16;
    readsz = 0;
    readp = buf = malloc(bufsz * sizeof(char));
    
    while (read(fd, readp, 1) > 0) {
        if (*readp == '\n')
            break;
        
        readp++;
        if (++readsz == bufsz) {
            buf = realloc(buf, bufsz *= 2);
            readp = buf + readsz;
        }
    }
    
    if (readsz > 0) {
        *readp = '\0';
        json = cJSON_Parse(buf);
        if (json == NULL)
            error("malformed JSON(%s): %s", cJSON_GetErrorPtr(), buf);
    }
    free(buf);
    return json;
}

static void write_msg(int fd, cJSON *msg)
{
    char nl = '\n';
    char *msg_str;
    
    msg_str = cJSON_PrintUnformatted(msg);
    
    write(fd, msg_str, strlen(msg_str));
    write(fd, &nl, 1);
    
    free(msg_str);
}